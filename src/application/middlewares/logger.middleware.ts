import { Request, Response, NextFunction } from 'express';

const logger = (req: Request, res: Response, next: NextFunction) => {
  const { url, method } = req;
  console.log(`>>> ${method} ${url}`);

  next();
};

export default logger;
