import multer from 'multer';
import path from 'path';

const multerDestination = path.join(__dirname, '../../', 'temp');

const multerConfig = multer.diskStorage({
  destination: multerDestination,
  filename: (req, file, callback) => {
    callback(null, file.originalname);
  },
});

const uploadMiddleWare = multer({ storage: multerConfig });

export default uploadMiddleWare;
