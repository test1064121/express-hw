import Joi from 'joi';

export const idParamSchema = Joi.object<{ id: string }>({
  // id: Joi.string().hex().length(24).required(),
  id: Joi.string().pattern(/^[1-9][0-9]*$/).required(),
});
