import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/groups.router';
import bodyParser from 'body-parser';
import exceptionFilter from './middlewares/exceptions.filter';
import path from 'path';
import { AppDataSource } from '../configs/database/data-source';
import lectorRouter from '../lectors/lectors.router';
import courseRouter from '../courses/courses.router';
import markRouter from '../marks/marks.router';
import lectorCourseRouter from '../lector-course/lector-course.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);
AppDataSource.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch((error) => {
    console.log('Error: ', error);
  });

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);

app.use('/lectors', lectorRouter);
app.use('/courses', courseRouter);
app.use('/marks', markRouter);
app.use('/lector-course', lectorCourseRouter);

app.use(exceptionFilter);

export default app;
