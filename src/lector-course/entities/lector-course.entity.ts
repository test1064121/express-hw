import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'lector_course' })
export class LectorCourse {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Lector, (lector) => lector.courses, { nullable: false })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @ManyToOne(() => Course, (course) => course.lectors, { nullable: false })
  @JoinColumn({ name: 'course_id' })
  course: Course;
}
