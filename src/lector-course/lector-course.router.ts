import express from 'express';
import { lectorCourseCreateSchema } from './lector-course.schema';
import * as lectorCourseController from './lector-course.controller';
import validator from '../application/middlewares/validation.middleware';

const router = express.Router();

router.post(
  '/',
  validator.body(lectorCourseCreateSchema),
  lectorCourseController.addLectorToCourse,
);
router.delete('/:id', lectorCourseController.removeLectorFromCourse);

export default router;
