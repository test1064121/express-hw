import { Request, Response } from 'express';
import * as lectorCourseService from './lector-course.service';

export const addLectorToCourse = async (req: Request, res: Response) => {
  const { lectorId, courseId } = req.params;
  const result = await lectorCourseService.addLectorToCourse(lectorId, courseId);
  res.json(result);
};


export const removeLectorFromCourse = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    await lectorCourseService.deleteLectorFromCourse(id);
    res.json({ message: 'Lector removed from course' });
  } catch (error) {
    res.status(500).json({ error: 'Error. Lector was not removed from course' });
  }
};
