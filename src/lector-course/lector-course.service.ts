import { getRepository } from 'typeorm';
import { LectorCourse } from './entities/lector-course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Course } from '../courses/entities/course.entity';

const lectorCourseRepository = getRepository(LectorCourse);

export const addLectorToCourse = async (
  lectorId: string,
  courseId: string,
): Promise<LectorCourse> => {
  const lectorCourseRepository = getRepository(LectorCourse);
  const lectorCourse = new LectorCourse();
  lectorCourse.lector = { id: lectorId } as Lector;
  lectorCourse.course = { id: courseId } as Course;
  const result = await lectorCourseRepository.save(lectorCourse);
  return result;
};

export const deleteLectorFromCourse = async (
  lectorCourseId: string,
): Promise<void> => {
  await lectorCourseRepository.delete(lectorCourseId);
};
