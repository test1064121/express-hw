import Joi from 'joi';

export const lectorCourseCreateSchema = Joi.object({
  lectorId: Joi.string().required(),
  courseId: Joi.string().required(),
});
