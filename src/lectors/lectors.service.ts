import { getRepository } from 'typeorm';
import { Lector } from './entities/lector.entity';
import HttpException from '../application/exceptions/http-exception';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

const lectorsRepository = getRepository(Lector);

export const createLector = async (
  lectorData: Partial<Lector>,
): Promise<Lector> => {
  const lector = await lectorsRepository.save(lectorData);
  return lector;
};

export const getAllLectors = async (): Promise<Lector[]> => {
  const lectors = await lectorsRepository.find();
  return lectors;
};

export const getLectorWithCourses = async (
  lectorId: string,
): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'course')
    .where('lector.id = :lectorId', { lectorId })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector;
};
