import express from 'express';
import { lectorCreateSchema } from './lectors.schema';
import * as lectorsController from './lectors.controller';
import validator from '../application/middlewares/validation.middleware';
import controllerWrapper from '../application/utilities/controller-wrapper';
import { idParamSchema } from '../application/schemas/id-param-schema';

const router = express.Router();

router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);
router.get('/', controllerWrapper(lectorsController.getAllLectors));
router.get('/:lectorId', validator.params(idParamSchema), lectorsController.getLectorWithCourses);

export default router;
