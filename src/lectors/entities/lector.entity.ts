import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { LectorCourse } from '../../lector-course/entities/lector-course.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'lectors' })
export class Lector {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ type: 'varchar', nullable: false })
  email: string;

  @Column({ type: 'varchar', nullable: false })
  password: string;

  @OneToMany(() => LectorCourse, (lectorCourse) => lectorCourse.lector)
  courses: LectorCourse[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
