import { Request, Response } from 'express';
import * as lectorService from './lectors.service';
import { ICreateLectorRequest } from './types/lector-create-request.interface';
import { lectorCreateSchema } from './lectors.schema';

export const createLector = async (
  req: Request<ICreateLectorRequest>,
  res: Response,
) => {
  const lectorData = req.body;
  await lectorCreateSchema.validateAsync(lectorData);
  const lector = await lectorService.createLector(lectorData);
  res.json(lector);
};

export const getAllLectors = async (req: Request, res: Response) => {
  const lectors = await lectorService.getAllLectors();
  res.json(lectors);
};

export const getLectorWithCourses = async (
  req: Request<{ lectorId: string }>,
  res: Response,
) => {
  const { lectorId } = req.params;
  const lector = await lectorService.getLectorWithCourses(lectorId);
  res.json(lector);
};
