import { getRepository } from 'typeorm';
import { Course } from './entities/course.entity';

const coursesRepository = getRepository(Course);

export const createCourse = async (
  courseData: Partial<Course>,
): Promise<Course> => {
  const course = coursesRepository.create(courseData);
  return coursesRepository.save(course);
};

export const updateCourse = async (
  courseId: string,
  courseData: Partial<Course>,
): Promise<Course | undefined> => {
  await coursesRepository.update(courseId, courseData);
  const updatedCourse = await coursesRepository.findOne({
    where: { id: courseId },
  });
  return updatedCourse || undefined;
};

export const deleteCourse = async (courseId: string): Promise<void> => {
  await coursesRepository.delete(courseId);
};

export const getCourses = async (): Promise<Course[]> => {
  return coursesRepository.find();
};

export const getCourseById = async (
  courseId: string,
): Promise<Course | undefined> => {
  const course = await coursesRepository.findOne({ where: { id: courseId } });
  return course || undefined;
};
