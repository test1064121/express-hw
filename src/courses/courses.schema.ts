import Joi from 'joi';

export const createCourseSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.string().allow('').optional(),
  hours: Joi.number().integer().positive().required(),
});

export const updateCourseSchema = Joi.object({
  name: Joi.string().optional(),
  description: Joi.string().allow('').optional(),
  hours: Joi.number().integer().positive().optional(),
});
