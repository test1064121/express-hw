import express from 'express';
import * as coursesController from './courses.controller';
import { createCourseSchema, updateCourseSchema } from './courses.schema';
import validator from '../application/middlewares/validation.middleware';

const router = express.Router();

router.get('/', coursesController.getCourses);
router.get('/:id', coursesController.getCourseById);
router.post(
  '/',
  validator.body(createCourseSchema),
  coursesController.createCourse,
);
router.put(
  '/:id',
  validator.body(updateCourseSchema),
  coursesController.updateCourse,
);
router.delete('/:id', coursesController.deleteCourse);

export default router;
