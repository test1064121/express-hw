import { Request, Response } from 'express';
import { Course } from './entities/course.entity';
import { createCourseSchema, updateCourseSchema } from './courses.schema';
import { getRepository } from 'typeorm';

const coursesRepository = getRepository(Course);

export const createCourse = async (req: Request, res: Response) => {
  try {
    const { error } = createCourseSchema.validate(req.body);
    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }

    const courseData = req.body;
    const course = coursesRepository.create(courseData);
    const createdCourse = await coursesRepository.save(course);
    res.json(createdCourse);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error. Course was not created' });
  }
};

export const updateCourse = async (req: Request, res: Response) => {
  try {
    const courseId = req.params.id;
    const { error } = updateCourseSchema.validate(req.body);
    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }

    const courseData = req.body;
    await coursesRepository.update(courseId, courseData);
    const updatedCourse = await coursesRepository.findOne({
      where: { id: courseId },
    });
    res.json(updatedCourse);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error. Course was not updated' });
  }
};

export const deleteCourse = async (req: Request, res: Response) => {
  try {
    const courseId = req.params.id;
    await coursesRepository.delete(courseId);
    res.sendStatus(204);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error. Course was not deleted' });
  }
};

export const getCourses = async (req: Request, res: Response) => {
  try {
    const courses = await coursesRepository.find();
    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error. Course could not be retrieve' });
  }
};

export const getCourseById = async (req: Request, res: Response) => {
  try {
    const courseId = req.params.id;
    const course = await coursesRepository.findOne({ where: { id: courseId } });
    if (!course) {
      return res.status(404).json({ error: 'Course not found' });
    }
    res.json(course);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error. Course could not be retrieve' });
  }
};
