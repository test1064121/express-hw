import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { LectorCourse } from '../../lector-course/entities/lector-course.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'courses' })
export class Course {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ type: 'text', nullable: true })
  description: string;

  @Column({ type: 'integer', nullable: false })
  hours: number;

  @OneToMany(() => LectorCourse, (lectorCourse) => lectorCourse.course)
  lectors: LectorCourse[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
