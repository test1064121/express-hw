import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/student.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, FindOneOptions, UpdateResult } from 'typeorm';

const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (): Promise<Student[]> => {


  const students = await studentsRepository
    .createQueryBuilder('student')
    .leftJoin('student.group', 'group')
    .addSelect('group.name')
    .getMany();

  return students;
};

export const getStudentById = async (id: string): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.status as status',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  studentCreateSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: studentCreateSchema.email,
    },
  });

  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }
  return studentsRepository.save(studentCreateSchema);
};

export const updateStudentById = async (
  id: string,
  studentUpdateSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, studentUpdateSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};

export const addGroup = async (
  studentId: string,
  groupId: string,
): Promise<UpdateResult> => {
  const studentOptions: FindOneOptions<Student> = {
    where: { id: parseInt(studentId) },
  };

  const student = await studentsRepository.findOne(studentOptions);

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  student.groupId = groupId;

  return await studentsRepository.update(studentId, student);
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};