import { Status } from '../../application/enums/status.enum';

export interface IStudent {
  id: number;
  email: string;
  name: string;
  surname: string;
  status: Status;
  age: number;
  // imagePath?: string;
  // groupId?: string;
}
