import Joi from 'joi';
import { IStudent } from './types/student.interface';
import { Status } from '../application/enums/status.enum';

export const studentCreateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().required(),
  age: Joi.number().required(),
  surname: Joi.string().required(),
  status: Joi.string()
    .valid(...Object.values(Status))
    .required(),
  email: Joi.string().required(),
});

export const studentUpdateSchema = Joi.object<Partial<IStudent>>({
  name: Joi.string().optional(),
  age: Joi.number().optional(),
  surname: Joi.string().optional(),
  status: Joi.string()
    .valid(...Object.values(Status))
    .optional(),
  email: Joi.string().optional(),
});
