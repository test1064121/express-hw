import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IGroup } from './types/group.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import { DeleteResult } from 'typeorm';

const groupsRepository = AppDataSource.getRepository(Group);


export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .getMany();
  return groups;
};

export const getGroupById = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return group;
};

export const createGroup = async (groupCreateSchema: Omit<IGroup, 'id'>): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: groupCreateSchema.name,
    },
  });

  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exists',
    );
  }
  return groupsRepository.save(groupCreateSchema);
};

export const updateGroupById = async (
  id: string,
  groupUpdateSchema: Partial<IGroup>,
) => {
  const result = await groupsRepository.update(id, groupUpdateSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return result;
};

export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
  const result = await groupsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return result;
  
};

