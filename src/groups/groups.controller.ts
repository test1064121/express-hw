import { Request, Response } from 'express';
import * as groupsService from './groups.service';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupCreateRequest } from './types/group-create-request.interface';

export const getAllGroups = async (req: Request, res: Response) => {
  const groups = await groupsService.getAllGroups();
  res.json(groups);
};

export const getGroupById = async (
  req: Request<{ id: string }>,
  res: Response,
) => {
  const groupWithStudents = await groupsService.getGroupById(req.params.id);
  res.json(groupWithStudents);
};

export const createGroup = async (
  req: ValidatedRequest<IGroupCreateRequest>,
  res: Response,
) => {
  console.log(req.body);
  const group = await groupsService.createGroup(req.body);
  res.json(group);
};

export const updateGroupById = async (
  req: ValidatedRequest<IGroupUpdateRequest>,
  res: Response,
) => {
  const { id } = req.params;
  const group = await groupsService.updateGroupById(id, req.body);
  res.json(group);
};

export const deleteGroupById = async (
  req: Request<{ id: string }>,
  res: Response,
) => {
  const { id } = req.params;
  const group = await groupsService.deleteGroupById(id);
  res.json(group);
};
