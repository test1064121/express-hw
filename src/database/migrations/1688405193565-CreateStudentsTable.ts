import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateStudentsTable1688405193565 implements MigrationInterface {
    name = 'CreateStudentsTable1688405193565'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."students_status_enum" AS ENUM('ACTIVE', 'PAUSSED')`);
        await queryRunner.query(`CREATE TABLE "students" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying, "age" numeric, "surname" character varying, "salary" numeric, "status" "public"."students_status_enum" NOT NULL DEFAULT 'ACTIVE', CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "students"`);
        await queryRunner.query(`DROP TYPE "public"."students_status_enum"`);
    }

}
