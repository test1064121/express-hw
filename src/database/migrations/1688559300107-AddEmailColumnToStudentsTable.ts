import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddEmailColumnToStudentsTable1688559300107
  implements MigrationInterface
{
  name = 'AddEmailColumnToStudentsTable1688559300107';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" ADD "email" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "email"`);
  }
}
