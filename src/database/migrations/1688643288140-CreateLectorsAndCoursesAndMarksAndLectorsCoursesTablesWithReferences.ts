import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateLectorsAndCoursesAndMarksAndLectorsCoursesTablesWithReferences1688643288140 implements MigrationInterface {
    name = 'CreateLectorsAndCoursesAndMarksAndLectorsCoursesTablesWithReferences1688643288140'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "marks" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "mark" integer NOT NULL, "course_id" uuid NOT NULL, "student_id" integer NOT NULL, "lector_id" uuid NOT NULL, CONSTRAINT "PK_051deeb008f7449216d568872c6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "lectors" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, CONSTRAINT "PK_87eda9bf8c85d84a6b18dfc4991" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "lector_course" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "lector_id" uuid NOT NULL, "course_id" uuid NOT NULL, CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "courses" ADD "description" text`);
        await queryRunner.query(`ALTER TABLE "courses" ADD "hours" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "courses" DROP CONSTRAINT "courses_pkey"`);
        await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "courses" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "courses" ADD CONSTRAINT "PK_3f70a487cc718ad8eda4e6d58c9" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "courses" ADD "name" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`);
        await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "courses" ADD "name" character varying(50)`);
        await queryRunner.query(`ALTER TABLE "courses" DROP CONSTRAINT "PK_3f70a487cc718ad8eda4e6d58c9"`);
        await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "courses" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "courses" ADD CONSTRAINT "courses_pkey" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "hours"`);
        await queryRunner.query(`ALTER TABLE "courses" DROP COLUMN "description"`);
        await queryRunner.query(`DROP TABLE "lector_course"`);
        await queryRunner.query(`DROP TABLE "lectors"`);
        await queryRunner.query(`DROP TABLE "marks"`);
    }

}
