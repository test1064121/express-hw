import { DataSource } from 'typeorm';
import { databaseCongiguration } from './database-config';

export const AppDataSource = new DataSource(databaseCongiguration());
