import { getRepository } from 'typeorm';
import { Mark } from './entities/mark.entity';
import { Request } from 'express';

const marksRepository = getRepository(Mark);

export const createMark = async (req: Request): Promise<Mark> => {
  const { courseId, studentId, lectorId, mark } = req.body;
  const newMark = marksRepository.create({
    course: { id: courseId },
    student: { id: studentId },
    lector: { id: lectorId },
    mark: mark,
  });
  return marksRepository.save(newMark);
};

export const getAllMarks = async (): Promise<Mark[]> => {
  return marksRepository.find();
};

export const getMarkById = async (id: string): Promise<Mark | undefined> => {
  const mark = await marksRepository.findOne({ where: { id } });
  return mark || undefined;
};
