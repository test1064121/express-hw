import Joi from 'joi';

export const markCreateSchema = Joi.object({
  courseId: Joi.string().required(),
  studentId: Joi.string().required(),
  lectorId: Joi.string().required(),
  mark: Joi.number().integer().min(0).max(100).required(),
});
