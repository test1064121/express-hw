import express from 'express';
import { markCreateSchema } from './marks.schema';
import * as marksController from './marks.controller';
import validator from '../application/middlewares/validation.middleware';
import controllerWrapper from '../application/utilities/controller-wrapper';
import { idParamSchema } from '../application/schemas/id-param-schema';

const router = express.Router();

router.post('/', validator.body(markCreateSchema), controllerWrapper(marksController.postMark));
router.get('/', controllerWrapper(marksController.getMarks));
router.get('/:markId', validator.params(idParamSchema), marksController.getMark);

export default router;