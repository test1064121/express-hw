export interface ICreateMarkRequest {
  courseId: string;
  studentId: string;
  lectorId: string;
  mark: number;
}
