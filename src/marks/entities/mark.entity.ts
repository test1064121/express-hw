import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'marks' })
export class Mark {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'integer', nullable: false })
  mark: number;

  @ManyToOne(() => Course, (course) => course.marks, { nullable: false })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Student, (student) => student.marks, { nullable: false })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ManyToOne(() => Lector, (lector) => lector.marks, { nullable: false })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
