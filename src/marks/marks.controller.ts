import { Request, Response } from 'express';
import { createMark, getAllMarks, getMarkById } from './marks.service';

export const postMark = async (req: Request, res: Response) => {
  try {
    const mark = await createMark(req.body);
    res.json(mark);
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong' });
  }
};

export const getMarks = async (req: Request, res: Response) => {
  try {
    const marks = await getAllMarks();
    res.json(marks);
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong' });
  }
};

export const getMark = async (req: Request, res: Response) => {
  try {
    const mark = await getMarkById(req.params.id);
    if (!mark) {
      res.status(404).json({ error: 'Mark not found' });
    } else {
      res.json(mark);
    }
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong' });
  }
};